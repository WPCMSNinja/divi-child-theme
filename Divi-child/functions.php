<?php

if (!function_exists('enqueue_divi_child_theme_styles') ) {
    function enqueue_divi_child_theme_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css', false, filemtime( get_stylesheet_directory() . '/style.css' ), 'all' );
        wp_enqueue_style( 'child-style', get_stylesheet_uri(), array('parent-style'), filemtime( get_stylesheet_directory() . '/style.css' ), 'all'  );
    }
    add_action( 'wp_enqueue_scripts', 'enqueue_divi_child_theme_styles' );
}

add_action( 'wp_print_styles', 'et_deregister_styles', 20 );
function et_deregister_styles() {
	wp_dequeue_style( 'divi-style' );
}